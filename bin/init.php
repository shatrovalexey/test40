<?php
	$action = getopt( 'a:c:f' ) ;
	$app = new \Application\Application( $action[ 'c' ] ) ;
	$args = json_decode( file_get_contents( $action[ 'f' ] ) ) ;
	echo json_encode( $action[ 'a' ] , $app->execute( $args ) ) ;