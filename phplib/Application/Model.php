<?php
	namespace Application ;

	/**
	* Модель
	* @author Shatrov Aleksej <mail@ashatrov.ru>
	*/
	class Application\Model {
		/**
		* @var \Application\Application $app - фасад проекта
		*/
		protected $app ;

		/**
		* Конструктор
		* @param \Application\Application $app - фасад проекта
		*/
		public function __construct( $app ) {
			$this->app = $app ;
		}

		/**
		* Импорт данных
		* @param array $args - аргументы командной строки
		* @return boolean - результат выполнения
		*/
		public function import( $args ) {
			return $this->app->dbh->exec( "
DROP TEMPORARY IF EXISTS `shops` ;

CREATE TABLE `shops`(
	`id` BIGINT( 22 ) UNSIGNED NOT null AUTOINCREMENT ,
	`region_id` BIGINT( 22 ) UNSIGNED NOT null COMMENT 'уникальный идентификатор магазина, присваивается автоматически' ,
	`title` VARCHAR( 100 ) CHARSET utf8mb4 NOT null COMMENT 'название магазина' ,
	`city` VARCHAR( 100 ) CHARSET utf8mb4 NOT null COMMENT 'идентификатор региона' ,
	`address` VARCHAR( 100 ) CHARSET utf8mb4 NOT null COMMENT 'адрес' ,
	`user_id` BIGINT( 22 ) UNSIGNED NOT null COMMENT 'менеджер'
) ENGINE = InnoDB , COMMENT 'магазин' ;
LOAD DATA INFILE
	'{$args[ 'file_name' ]}'
IGNORE
INTO TABLE
	`shops`
COLUMNS
	TERMINATED BY ','
	OPTIONALLY ENCLOSED BY '\"'
	ESCAPED BY '\"'
LINES
	TERMINATED BY '\\n'
IGNORE 1 LINES
	( `region_id` , `title` , `city` , `address` , `user_id` ) ;
			" ) ;
		}

		/**
		* Установка аргументов
		* @param array $args - список полей
		* @return \Application\Model - объект модели
		*/
		public function set( $args ) {
			foreach ( $args as $key => $value ) {
				$this->$key = $value ;
			}

			return $this ;
		}

		/**
		* Поиск записи по идентификатору
		* @param array $args - список полей
		* @return \Application\Model - объект модели
		*/
		public function get( $args ) {
			$sth = $this->app->dbh->prepare( '
SELECT
	`s1`.*
FROM
	`shop` AS `s1`
WHERE
	( `s1`.`id` = ? ) ;
			' ) ;
			$sth->execute( array( $args[ 'id' ] ) ) ;

			foreach ( $sth->fetch( \PDO::FETCH_ASSOC ) as $key => $value ) {
				$this->$key = $value ;
			}

			return $this ;
		}

		/**
		* Валидация списка полей
		* @param array $args - список полей
		* @return \Application\Model - объект модели
		*/
		public function validate( $args ) {
			$this->app->dbh->beginTransaction( ) ;
			$result = $this->save( $args ) ;
			$this->app->dbh->rollBack( ) ;

			return $result ;
		}

		/**
		* Запись записи в БД
		* @param array $args - список полей
		* @return boolean - результат выполнения
		*/
		public function save( $args ) {
			try {
				$this->app->dbh->prepare( '
REPLACE INTO
	`shops`
SET
	`id` := :id ,
	`region_id` := :region_id ,
	`title` := :title ,
	`city` := :city ,
	`address` := :address ,
	`user_id` := :user_id ;
				' )->execute( $args ) ;
			} catch ( \Exception as $exception ) {
				return false ;
			}
			return true ;
		}
	}