<?php
	namespace Application ;

	/**
	* Модель
	* @author Shatrov Aleksej <mail@ashatrov.ru>
	*/
	class Application\Application {
		/**
		* @var stdClass $config - настройки
		* @var \PDO $dbh - подключение к СУБД
		* @var array $args - аргументы командной строки
		*/
		protected $config ;
		protected $dbh ;
		protected $args ;

		/**
		* Конструктор
		* @param string $configFileName - файл настроек
		*/
		public function __construct( $configFileName ) {
			$this->prepare( $configFileName ) ;
		}

		/**
		* Подготовка
		* @param string $configFileName - файл настроек
		* @return \Application\Application
		*/
		protected function prepare( $configFileName ) {
			$this->config = json_decode( file_get_contents( $configFileName ) ) ;
			$this->dbh = new \PDO(
				sprintf( $this->config->dbh->pdo_dsn , $this->config->dbh->host , $this->config->dbh->database ) ,
				$this->config->dbh->login , $this->config->dbh->passwd ,
				array(
					\PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
					\PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES ' . $this->config->dbh->charset
				)
			) ;

			return $this ;
		}

		/**
		* Выполнение
		* @param array $args - аргументы командной строки
		* @return \Application\Application
		*/
		public function execute( $args ) {
			return ( new \Application\Model( $this ) )->execute( $args[ 'action' ] , $args ) ;
		}
	}