Импорт данных:
```
DROP TEMPORARY IF EXISTS `shops` ;
CREATE TABLE `shops`(
	`id` BIGINT( 22 ) UNSIGNED NOT null AUTOINCREMENT ,
	`region_id` BIGINT( 22 ) UNSIGNED NOT null COMMENT 'уникальный идентификатор магазина, присваивается автоматически' ,
	`title` VARCHAR( 100 ) CHARSET utf8mb4 NOT null COMMENT 'название магазина' ,
	`city` VARCHAR( 100 ) CHARSET utf8mb4 NOT null COMMENT 'идентификатор региона' ,
	`address` VARCHAR( 100 ) CHARSET utf8mb4 NOT null COMMENT 'адрес' ,
	`user_id` BIGINT( 22 ) UNSIGNED NOT null COMMENT 'менеджер'
) ENGINE = InnoDB , COMMENT 'магазин' ;

LOAD DATA INFILE
	'shops.csv'
IGNORE
INTO TABLE
	`shops`
COLUMNS
	TERMINATED BY ','
	OPTIONALLY ENCLOSED BY '"'
	ESCAPED BY '"'
LINES
	TERMINATED BY '\n'
IGNORE 1 LINES
	( `region_id` , `title` , `city` , `address` , `user_id` ) ;
```

validate:
```
START TRANSACTION ;
INSERT INTO
	`shops`
SET
	`region_id` := @`region_id` ,
	`title` := @`title` ,
	`city` := @`city` ,
	`address` := @`address` ,
	`user_id` := @`user_id` ;
ROLLBACK ;
```

get:
SELECT SQL_SMALL_RESULT SQL_BUFFER_RESULT SQL_NO_CACHE
	`s1`.*
FROM
	`shops` AS `s1`
WHERE
	( `s1`.`id` = @`id` ) ;

set:
UPDATE IGNORE
	`shops` AS `s1`
SET
	`s1`.`region_id` := @`region_id` ,
	`s1`.`title` := @`title` ,
	`s1`.`city` := @`city` ,
	`s1`.`address` := @`address` ,
	`s1`.`user_id` := @`user_id` ;

save:
INSERT INTO
	`shops`
SET
	`region_id` := @`region_id` ,
	`title` := @`title` ,
	`city` := @`city` ,
	`address` := @`address` ,
	`user_id` := @`user_id` ;